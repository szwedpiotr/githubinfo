package pl.test.githubinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class GithubinfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GithubinfoApplication.class, args);
    }
}
