package pl.test.githubinfo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.test.githubinfo.exception.UnsupportedRequestException;
import pl.test.githubinfo.service.EndpointService;

@RestController
@RequestMapping("/repositories")
public class EndpointController  {

    private static final Logger log = LoggerFactory.getLogger(EndpointController.class);

    private final EndpointService endpointService;

    public EndpointController(EndpointService endpointService) {
        this.endpointService = endpointService;
    }

    @RequestMapping(value = "/{owner}/{repositoryName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getRepositoriesByOwnerAndRepositoryName(@PathVariable String owner, @PathVariable String repositoryName) {
        log.info("Request for owner {} and repository {}.", owner, repositoryName);
        return new ResponseEntity<>(endpointService.getGithubInfoByOwnerAndRepositoryName(owner, repositoryName), HttpStatus.OK);
    }

    @RequestMapping(value = "/{owner}/{repositoryName}",
            method = {RequestMethod.POST, RequestMethod.DELETE, RequestMethod.HEAD, RequestMethod.PUT, RequestMethod.OPTIONS},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> setRepositoriesByOwnerAndRepositoryName(@PathVariable String owner, @PathVariable String repositoryName) {
        log.info("Request for owner {} and repository {}.", owner, repositoryName);
        throw new UnsupportedRequestException();
    }
}
