package pl.test.githubinfo.enums;

public enum ExceptionSeverity {
    INFO,
    WARNING,
    ERROR
}
