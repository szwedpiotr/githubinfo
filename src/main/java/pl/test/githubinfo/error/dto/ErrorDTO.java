package pl.test.githubinfo.error.dto;

import java.io.Serializable;

@SuppressWarnings("LocalCanBeFinal")
public class ErrorDTO implements Serializable {

    private final String error;

    public ErrorDTO(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    @Override
    public String toString() {
        return "ErrorDTO{error='" + error + "'}";
    }
}
