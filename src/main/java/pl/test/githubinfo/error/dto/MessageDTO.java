package pl.test.githubinfo.error.dto;

import java.io.Serializable;

@SuppressWarnings("LocalCanBeFinal")
public class MessageDTO implements Serializable {

    private final String message;

    public MessageDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "MessageDTO{message='" + message + "'}";
    }
}

