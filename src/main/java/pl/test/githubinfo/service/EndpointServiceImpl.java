package pl.test.githubinfo.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.test.githubinfo.domain.GithubInfo;
import pl.test.githubinfo.exception.ApplicationException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static pl.test.githubinfo.converter.Converter.convertJsonNodeFromJsonString;
import static pl.test.githubinfo.converter.Converter.convertJsonNodeToGithubInfo;
import static pl.test.githubinfo.exception.ExceptionResolver.resolveException;

@Service
public class EndpointServiceImpl implements EndpointService {

    private static final Logger log = LoggerFactory.getLogger(EndpointServiceImpl.class);
    private final RestTemplate restTemplate;
    @Value("${github.uri}")
    private String githubUri;

    public EndpointServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public GithubInfo getGithubInfoByOwnerAndRepositoryName(final String owner, final String repositoryName) {

        log.info("Preparing data for owner {} and repository {}", owner, repositoryName);

        final String jsonString = getRepositoryDataFromAPI(owner, repositoryName);
        final JsonNode jsonNode;
        try {
            jsonNode = convertJsonNodeFromJsonString(jsonString);
        } catch (IOException e) {
            log.warn("An application exception occurred while processing data for owner {} and repository {}", owner, repositoryName);
            throw new ApplicationException("An application exception occurred while processing data");
        }
        return convertJsonNodeToGithubInfo(jsonNode);
    }

    private String getRepositoryDataFromAPI(final String owner, final String repositoryName) {
        Map<String, String> variables = new HashMap<>(2);
        variables.put("owner", owner);
        variables.put("repositoryName", repositoryName);

        String jsonString = null;
        try {
            jsonString = this.restTemplate.getForObject(githubUri, String.class, variables);
        } catch (HttpClientErrorException e) {
            resolveException(e.getStatusCode());
        }
        return jsonString;
    }
}
