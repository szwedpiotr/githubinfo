package pl.test.githubinfo.service;

import pl.test.githubinfo.domain.GithubInfo;

public interface EndpointService {
    GithubInfo getGithubInfoByOwnerAndRepositoryName(final String owner, final String repositoryName);
}
