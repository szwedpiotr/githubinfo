package pl.test.githubinfo.exception;


import pl.test.githubinfo.enums.ExceptionSeverity;

public class ApplicationException extends RuntimeException {
    protected String message;
    protected ExceptionSeverity severity;

    public ApplicationException(String m) {
        message = m;
        severity = ExceptionSeverity.ERROR;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getSeverity() {
        return this.severity.name();
    }

    public void setSeverity(ExceptionSeverity severity) {
        this.severity = severity;
    }
}
