package pl.test.githubinfo.exception;

public class RepositoryForbiddenException extends ApplicationException {
    public RepositoryForbiddenException() {
        super("Repository forbidden exception");
    }
}
