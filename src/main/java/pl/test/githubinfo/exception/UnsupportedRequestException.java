package pl.test.githubinfo.exception;

public class UnsupportedRequestException extends ApplicationException {
    public UnsupportedRequestException() {
        super("Unsupported request exception");
    }
}
