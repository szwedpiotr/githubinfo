package pl.test.githubinfo.exception;

public class BadRequestException extends ApplicationException {
    public BadRequestException() { super("Bad request exception for repository");
    }
}
