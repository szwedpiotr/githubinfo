package pl.test.githubinfo.exception;

public class RepositoryNotFoundException extends ApplicationException {
    public RepositoryNotFoundException() {
        super("Repository not found exception");
    }
}
