package pl.test.githubinfo.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class ExceptionResolver {

    private static final Logger log = LoggerFactory.getLogger(ExceptionResolver.class);

    public static void resolveException(final HttpStatus httpStatus) {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            log.warn("No repository found");
            throw new RepositoryNotFoundException();
        } else if (httpStatus == HttpStatus.BAD_REQUEST) {
            log.warn("Bad request");
            throw new BadRequestException();
        } else if (httpStatus == HttpStatus.FORBIDDEN) {
            log.warn("Forbidden");
            throw new RepositoryForbiddenException();
        }
    }
}
