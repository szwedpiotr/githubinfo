package pl.test.githubinfo.converter;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.test.githubinfo.domain.GithubInfo;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Converter {

    public Converter() {
        throw new UnsupportedOperationException();
    }

    public static GithubInfo convertJsonNodeToGithubInfo(final JsonNode jsonNode) {
        final Instant instant = Instant.parse(jsonNode.path("created_at").asText());
        return new GithubInfo(
                jsonNode.path("full_name").asText(),
                jsonNode.path("description").asText(),
                jsonNode.path("clone_url").asText(),
                jsonNode.path("stargazers_count").asInt(),
                LocalDateTime.ofInstant(instant, ZoneOffset.UTC).toLocalDate());
    }

    public static JsonNode convertJsonNodeFromJsonString(final String jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(jsonString);
        return mapper.readTree(parser);
    }
}
