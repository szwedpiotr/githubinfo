package pl.test.githubinfo.domain;

import java.time.LocalDate;

public class GithubInfo {

    private final String fullName;
    private final String description;
    private final String cloneUrl;
    private final int stars;
    private final LocalDate createdAt;

    public GithubInfo(String fullName, String description, String cloneUrl, int stars, LocalDate createdAt) {
        this.fullName = fullName;
        this.description = description;
        this.cloneUrl = cloneUrl;
        this.stars = stars;
        this.createdAt = createdAt;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public int getStars() {
        return stars;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }
}
