package pl.test.githubinfo.EndToEndTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import pl.test.githubinfo.domain.GithubInfo;
import pl.test.githubinfo.error.ErrorResponse;
import pl.test.githubinfo.exception.RepositoryNotFoundException;
import pl.test.githubinfo.exception.UnsupportedRequestException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GithubinfoApplicationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void repositoryExistTest() {
        //given
        final String basicUrl = "/repositories/lazarus-ps/only_for_test";

        //when
        final ResponseEntity<GithubInfo> responseEntity =
                restTemplate.getForEntity(basicUrl, GithubInfo.class);
        final GithubInfo githubInfo = responseEntity.getBody();

        //then
        assertAll( "GithubInfo",
                () -> assertEquals(HttpStatus.OK, responseEntity.getStatusCode()),
                () -> assertEquals("lazarus-ps/only_for_test", githubInfo.getFullName()),
                () -> assertEquals("https://github.com/lazarus-ps/only_for_test.git", githubInfo.getCloneUrl()),
                () -> assertEquals("Repository only for application test.", githubInfo.getDescription()),
                () -> assertEquals(LocalDate.of(2018, 8, 31), githubInfo.getCreatedAt()),
                () -> assertEquals(1, githubInfo.getStars())
        );
    }

    @Test
    public void repositoryDoesNotExistTest() {
        //given
        final String basicUrl = "/repositories/null/null";

        //when
        final ResponseEntity<?> responseEntity =
                restTemplate.getForEntity(basicUrl, ErrorResponse.class);

        //then
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        Throwable exception = assertThrows(RepositoryNotFoundException.class, () -> {throw new RepositoryNotFoundException();});
        assertEquals("Repository not found exception", exception.getMessage());
    }

    @Test
    public void repositoryExistButIncorrectPostMethodTest() {
        //given
        final String basicUrl = "/repositories/lazarus-ps/only_for_test";

        //when
        final ResponseEntity<?> responseEntity =
                restTemplate.postForEntity(basicUrl, ResponseEntity.class, ErrorResponse.class);

        //then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        Throwable exception = assertThrows(UnsupportedRequestException.class, () -> {throw new UnsupportedRequestException();});
        assertEquals("Unsupported request exception", exception.getMessage());
    }
}
